import('../util/Request.js');
import('../util/XML.js');

/**
 * Encapsulates file upload/download logic.
 * Assumes AWS has been initialized.
 */
class S3 {

    /**
     * Returns an AWS.S3 instance pointing to the specified bucket.
     * @param {String} bucketName
     * @return {AWS.S3}
     */
    static getBucket(bucketName) {
        return new AWS.S3({
            apiVersion: '2006-03-01',
            params: {
                Bucket: bucketName
            },
            s3ForcePathStyle: true,
            correctClockSkew: true,
        });
    }

    /**
     * Uploads a file to an S3 bucket.
     * @param {Element} element
     * @param {String} bucketName
     * @param {?String} key
     * @return {Promise}
     */
    static uploadFile(element, bucketName, key) {
        const files = element.files;

        if (files.length !== 1) {
            throw 'Expected 1 file, got: ' + files.length;
        }

        const file = files[0];
        const bucket = S3.getBucket(bucketName);
        const params = {
            Key: key || file.name,
            Body: file
        };

        return new Promise((resolve, reject) => {
            bucket.upload(params, (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(data);
                }
            });
        });
    }

    /**
     * Gets the public URI from an S3 object.
     * @private
     */
    static _getObjectURI(bucket, key) {
        key = encodeURIComponent(key); //.replace(/%20/g, '+');

        return 'https://s3.amazonaws.com/' + bucket + '/' + key;
    }

    /**
     * Lists the objects in an S3 bucket.
     * @param {String} bucket
     * @param {?String} prefix
     * @return {Promise<object>}
     */
    static listFiles(bucket, prefix) {
        prefix = String(prefix) || '';
        
        return new Promise((resolve, reject) => {
            this.getBucket(bucket).listObjects({
                Bucket: bucket,
                Prefix: prefix
            }, (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(data.Contents.map(result => {
                        return {
                            name: result.Key,
                            href: this._getObjectURI(bucket, result.Key),
                            lastModified: result.LastModified,
                        };
                    }));
                };
            });
        });
    }

    /**
     * Retrieves an object from S3.
     * @param {String} bucket
     * @param {String} key
     * @param {?Boolean} breakCache - Whether to disallow the browser from caching the response.
        Default false.
     * @return {Promise<object>}
     */
    static getObject(bucket, key, breakCache) {
        const params = {
            Key: key,
        };

        if (breakCache) {
            params.ResponseCacheControl = 'no-cache';
            params.ResponseExpires = 0;
        }

        return new Promise((resolve, reject) => {
            this.getBucket(bucket).getObject(params, (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    resolve({
                        name: key,
                        href: this._getObjectURI(bucket, key),
                        lastModified: data.LastModified,
                    });
                };
            });
        });
    }
}
