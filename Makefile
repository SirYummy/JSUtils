DOCUMENTATION=documentation
DOCS_TMP=docs-tmp
ENV=environment
PIP=$(shell which pip)
SLURP=Slurp/build_modules
SRC=./util
DIST=dist
TEST_TEMP=./test-tmp
MOCHA=./node_modules/.bin/nyc ./node_modules/.bin/mocha
MOCHA_TEST_LOCATION='$(TEST_TEMP)/{,!(ui)/**}/test_*.js'
SUDO=$(shell which sudo)

create_env:
	virtualenv -p python3.6 env

install:
	git submodule update --init --recursive

	$(PIP) install --upgrade .

	$(SUDO) apt-get update
	curl -sL https://deb.nodesource.com/setup_8.x | $(SUDO) bash -
	$(SUDO) apt-get install -y nodejs

	npm install

init:
	git submodule update --init --recursive

test:
	@# Runs tests.
	@echo "${GREEN}Running tests${NC}"

	@rm -rf $(TEST_TEMP)
	@mkdir $(TEST_TEMP)

	@$(SLURP)/slurp.py $(SRC) $(TEST_TEMP) \
	| $(SLURP)/fileLinker.py --fileExtensions=html,js,css \
	| $(SLURP)/spit.py

	@# Unit tests.
	@$(MOCHA) $(MOCHA_TEST_LOCATION) --recursive

	@# UI tests.
	# npx cypress run

	# @rm -rf $(TEST_TEMP)

update_submodules:
	git submodule update --recursive

docs:
	@# Creates JS documentation.
	@npx documentation build --shallow --infer-private --sort-order="alpha" --format=html --output=$(DOCUMENTATION) ./aws ./backend ./components ./math ./pwa ./storage ./util ./voodoo

	@rm -rf $(DOCS_TMP)