import('../util/Dom.js');
import('../util/Request.js');

/**
 * @class
 */
class Voodoo {

    /**
     * @constructor
     */
    constructor(uri) {
        this._uri = uri;
        this._data = null;
    }

    /**
     * @public
     * @param {String} uri
     */
    render() {
        Request.get(this._uri).then(data => {
            this._data = this._cleanData(data);

            this._renderNavbar();

            const title = this._getPageTitle();
            document.title = title;

            const page = this._getPage();

            this._renderContent(page);
        });
    }

    /**
     * @private
     */
     _titleCase(str) {
        str = str.toLowerCase().split(' ');

        for (var i = 0; i < str.length; i++) {
            str[i] = str[i].charAt(0).toUpperCase() + str[i].slice(1); 
        }
        
        return str.join(' ');
    }

    /**
     * @private
     */
    _renderNavbar() {
        const pages = Array.from(Object.keys(this._data.pages));
        const pageTitles = Array.from(new Set(pages.map(title => {
            return this._titleCase(title);
        })));
        const pageURLs = pageTitles.map(title => {
            if (title.toLowerCase() === 'home') return '/';

            return '/' + title.toLowerCase();
        });

        const rendered = (new NavbarComponent(pageTitles, pageURLs)).render();

        Dom.appendTo(document.body, rendered);
    }

    /**
     * @private
     */
    _cleanData(data) {
        const pattern = /\/\*(.|\n)*?\*\//g;
        data = data.replace(pattern, '');

        data = JSON.parse(data);

        Array.from(Object.keys(data.pages)).forEach(key => {
            data.pages[key.toLowerCase()] = data.pages[key];
        });

        return data;
    }

    /**
     * @private
     */
    _renderContent(page) {
        if (!page.content) {
            Dom.appendTo(document.body, `<p>Error! No "content" attribute on this page.
                You must define some content.</p>`);

            return;
        }
        page.content.forEach(item => {
            if (item.subtitle) {
                this._renderHeader(item.subtitle, 'h2');
            }
            if (item.text) {
                this._renderText(item);
            }
            if (item.banner) {
                this._renderBanner(item.banner);
            }
        });
    }

    /**
     * @private
     */
    _renderBanner(item) {
        const rendered = (new BannerComponent(item.image, item.text, item.height)).render();

        Dom.appendTo(document.body, rendered);
    }

    /**
     * @private
     */
    _getPage() {
        let path = window.location.pathname;

        if (path === '/') path = 'Home';

        path = path.replace(/\//g, '');

        return this._data.pages[path];
    }

    /**
     * @private
     */
    _getPageTitle() {
        let path = window.location.pathname;

        if (path === '/') path = 'Home';

        path = path.replace(/\//g, '');

        const titleOverride = this._data.pages[path].title;

        return titleOverride || path;
    }

    /**
     * @private
     */
    _renderText(node) {
        let p = document.createElement('p');

        Dom.setContents(p, node.text);

        Dom.appendTo(document.body, p);
    }

    /**
     * @private
     */
    _renderHeader(title, type) {
        type = type || 'h1';

        let h1 = document.createElement(type);

        Dom.setContents(h1, title);

        Dom.appendTo(document.body, h1);
    }
}

Voodoo.CLASS = {
    NAVBAR: 'VoodooNavbar',
};