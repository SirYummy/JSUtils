# JSUtils

[Read the docs](https://jdsutton.gitlab.io/JSUtils/)

JSUtils provides a robust and painless foundation for potentially expansive front
ends. It is written in ES6, and makes use of classes extensively. It also avoids
the intermixing of languages (like you find in JSX, for example). Because of this, 
it is very simple to compile and integrate into other platforms. 

This is useful because it allows the lion's share of computation to take
place on the client-side -- thus, reducing the server load significantly. It also
does away with the need to draw on loads of libraries. JSUtils offers diverse
functionality without being bloated with unnecessary stuff, which makes it fast and reliable. 

Specifically, JSUtils offers:

- [Template redering]()
- [Automated component & view generation]()
- [AWS Lambda management]()
- [Distributed computing]()
- [Asynch taskrunner]()
- [S3 file Storage]()
- [Hot reload dev server]()
- [Common JS utilities]()

Some modules require a [Slurp](https://github.com/jdsutton/Slurp) build.

## Installation

**Clone the repo:**

```bash
git clone https://gitlab.com/jdsutton/JSUtils.git
```
```bash
git@gitlab.com:jdsutton/JSUtils.git
```

>NOTE: Install **Python3.6** if not already installed. 

 **Create a [virtualenv](https://pypi.org/project/virtualenv/) and install.**

```bash
make create_env

. env/bin/activate

make install
```

## Testing

Tests are driven by `Mocha` and a custom assertion library. 

```bash
make test
```

## Building the docs

```bash
make docs
```

Viewing the docs: 

```bash
cd documentation && python3 -m http.server
```

Then navigate to `localhost:8000` to view the docs. 

## Generate a component

```bash
./src/JSUtils/scripts/create_component.sh dir/ACoolComponent
```

Output will be under `./src/components/dir/ACoolComponent`

## Generate a view

```bash
./src/JSUtils/scripts/create_view.sh dir/myview MyCoolView
```
