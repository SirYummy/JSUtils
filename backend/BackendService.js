import('../util/HTMLString.js');
import('../util/Request.js');

/**
 * @class
 */
class BackendServiceProxyHandler {

    /**
     * @public
     */
    static get(obj, property) {
        if (property in obj) {
            return obj[property];
        }

        return (...args) => {
            let kwargs = args[args.length - 1];
            const a = args.slice(0, args.length - 1);
            const unsafe = Boolean(kwargs.unsafe);

            if (obj._dataGetter) {
                Object.assign(kwargs, obj._dataGetter());
            }

            return obj.invoke(property, a, kwargs, unsafe);
        }
    }
}

/**
 * @class
 */
class BackendService {

    /**
     * @constructor
     */
    constructor(lambdaFunctionName, methodPrefix, dataClass) {
        this._lambdaFunctionName = lambdaFunctionName;
        this._methodPrefix = methodPrefix || 'default';
        this._dataClass = dataClass || null;
        this._dataGetter = null;
    }

    /**
     * @public
     */
    forAllRequests(callback) {
        this._dataGetter = callback;

        return this;
    }

    static declare(lambdaFunctionName, dataClass) {
        const service = new BackendService(lambdaFunctionName, null, dataClass);

        return new Proxy(service, BackendServiceProxyHandler);
    }

    /**
     * @private
     * Escapes an object or list of objects for safety.
     */
    static _escape(obj) {
        if (typeof obj === 'string') {
            return HTMLString.escape(obj);
        } else if (Array.isArray(obj)) {
            return obj.map(item => {
                return this._escape(item);
            });
        } else if (obj !== null && typeof obj === 'object') {
            const keys = Array.from(Object.keys(obj));

            keys.forEach(key => {
                obj[key] = this._escape(obj[key]);
            });

            return obj;
        } else {
            return obj;
        }
    }

    /**
     * @public
     */
    invoke(method, args, kwargs, unsafe) {
        unsafe = unsafe || false;
        method = this._methodPrefix + '.' + method;

        const params = {
            FunctionName: this._lambdaFunctionName,
            Payload: JSON.stringify({
                method,
                args,
                kwargs,
            }),
            cache: new Date().getTime(),
        };

        return Request.post(this.constructor._ENDPOINT, params, {}, true).then(response => {
            return this._handleResponse(response, unsafe);
        });
    }

    /**
     * @private
     */
    _handleResponse(response, unsafe) {
        const parsed = JSON.parse(response);
        let result;

        if (parsed && parsed.errorMessage) {
            throw new Error(parsed.errorMessage);
        }

        if (unsafe) {
            result = parsed;
        } else {
            result = this.constructor._escape(parsed);
        }

        if (this._dataClass) {
            if (Array.isArray(result)) {
                return result.map(x => new this._dataClass(x));
            }

            return new this._dataClass(result);
        }

        return result;
    }
}

BackendService._ENDPOINT = '[["backendEndpoint"]]';