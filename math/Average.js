/**
 * @class
 */
class Average {

    /**
     * @public
     * @param {Vector[]} vectors
     * @param {Number[]} weights
     */
    static weightedAverage(vectors, weights) {
        const totalWeight = weights.reduce((total, weight) => total + weight);
        let total = vectors[0].zero();

        for (let i=0; i < vectors.length; i++) {
            const vector = vectors[i];
            const weight = weights[i];

            total = total.add(vector.multiply(weight));
        }

        return total.divide(totalWeight);
    }
}