import('Vector.js');

/**
 * @class
 */
class Spring {

    /**
     * @constructor
     */
    constructor(width, height) {
        this.child = null;
        this.density = this.constructor.DENSITY;
        this._width = width;
        this._height = height;
        this._mass = width * height * this.density;
        this.angle = 0;
        this.angularVelocity = 0;
        this._linearVelocity = 0;
        this._linearPosition = this._height;
        this._damping = null;
        this._minAngle = null;
        this._maxAngle = null;
    }

    /**
     * @public
     * @param {Number} min
     * @param {Number} max
     * @return {Spring} - this
     */
    setAngleConstraints(min, max) {
        this._minAngle = min;
        this._maxAngle = max;

        return this;
    }

    /**
     * @public
     */
    setDensity(value) {
        this.density = value;
        this._mass = this._width * this._height * this.density;

        return this;
    }

    /**
     * @public
     */
    setDamping(value) {
        this._damping = 1.0 - value;

        return this;
    }
    
    /**
     * @public
     */
    addChild(c) {
        this.child = c;
    }
    
    /**
     * @public
     */
    addChildren(n) {
        if (n < 1) {
            return;
        }
        
        this.child = new Spring(this._mass);
        
        this.child.addChildren(n-1);
    }
    
    /**
     * @public
     */
    getMass() {
        let total = this._mass;
        
        if (this.child !== null) {
            total += this.child.getMass();
        }
        
        return total;
    }
    
    /**
     * @private
     */
    _getSpringForce() {
        return this.constructor.SPRING_CONSTANT * this.angle;
    }
    
    /**
     * @private
     */
    _getFrictionAccelerationAngular() {
        const frictionForce = this.constructor.FRICTION_CONSTANT * 5 * this.angularVelocity;
        let acc = frictionForce / this._mass;

        acc = Math.min(Math.abs(acc), Math.abs(this.angularVelocity));

        return Math.sign(this.angularVelocity) * acc;
    }

    /**
     * @private
     */
    _getFrictionAccelerationLinear() {
        const frictionForce = this.constructor.FRICTION_CONSTANT * this._linearVelocity;
        let acc = frictionForce / this._mass;

        return Math.sign(this._linearVelocity) * acc;
    }

    /**
     * @public
     */
    update() {
        this.angularVelocity += (this._getSpringForce() / this._mass);
        // this.applyForce(this._width / 2, this._height / 2, this.constructor.GRAVITY);
        // this.angularVelocity -= this._getFrictionAccelerationAngular();
        // this._linearVelocity -= this._getFrictionAccelerationLinear();

        if (this._damping) {
            this.angularVelocity *= this._damping;
            this._linearVelocity *= this._damping;
        }
        
        // Apply linear acceleration due to compression.
        const dx = this._linearPosition - this._height;
        const linearAcceleration = this.constructor.SPRING_CONSTANT * dx / this._mass;
        this._linearVelocity += linearAcceleration
        this._linearPosition += this._linearVelocity;

        this.angle += this.angularVelocity;

        if (this._minAngle !== null && this.angle < this._minAngle) {
            this.angle = this._minAngle;
            this.angularVelocity = 0;
        } else if (this._maxAngle !== null && this.angle > this._maxAngle) {
            this.angle = this._maxAngle;
            this.angularVelocity = 0;
        }
    }

    /**
     * @public
     * @return {Number}
     */
    getAngleDegrees() {
        return this.angle * (180.0 / Math.PI);
    }

    /**`
     * @public
     * @return {Number}
     */
    getCompressionDistance() {
        return this._height - this._linearPosition;
    }

    /**
     * @public
     */
    getLength() {
        return this._linearPosition;
    }

    /**
     * @public
     */
    getRestingLength() {
        return this._height;
    }

    /**
     * @public
     * @param {Number} x - X position of the point of contact.
     * @param {Number} y - Y position of the point of contact.
     * @param {Vector} force - Force vector.
     */
    applyForce(x, y, force) {
        // Compute distance of lever arm.
        const r = new Vector(x - this._width/2, y - this._height/2);

        // Compute component of force in direction of compression.
        const compressionAngle = force.angle() - (this.angle + Math.PI/2);

        // Apply compression.
        const compressionForce = Math.cos(compressionAngle) * force.magnitude();
        const resistanceForce = this.constructor.SPRING_CONSTANT * this.getCompressionDistance();
        if (resistanceForce < compressionForce) {
            const totalCompressionForce = compressionForce - resistanceForce;

            this._linearVelocity -= totalCompressionForce / this._mass;
        }

        // Compute angle between lever arm and force vector.
        const theta = force.angle() - r.angle();

        // Apply angular acceleration.
        const torque = r.magnitude() * force.magnitude() * Math.sin(theta);
        // Let moment of inertia = C * mass for laziness.
        const angularAcceleration = torque / (this.constructor.MOMENT_OF_INERTIA_CONSTANT
            * this._mass);
        
        this.angularVelocity += angularAcceleration;
    }
};

Spring.GRAVITY = new Vector([0, 0.1]);
Spring.SPRING_CONSTANT = -1000;
Spring.FRICTION_CONSTANT = 8000;
Spring.DENSITY = 2.5;
Spring.MOMENT_OF_INERTIA_CONSTANT = 1.0;