/**
 * @class
 */
class Matrix {

    /**
     * @constructor
     */
    constructor(data) {
        const args = Array.from(arguments);

        if (args.length > 1) {
            this._data = [];

            args.forEach(arg => {this._data.push(arg)});
        } else {
            this._data = data || [];
        }
    }

    /**
     * @public
     */
    get columns() {
        if (!this._data[0]) return 0;

        return this._data[0].length;
    }

    /**
     * @public
     */
    get rows() {
        return this._data.length;
    }

    /**
     * @public
     */
    getColumn(i) {
        let result = [];

        for (let j = 0; j < this._data.length; j++) {
            result.push(this._data[j][i]);
        }

        return result;
    }

    /**
     * @public
     */
    multiply(b) {
        let result = [];

        if (this.columns !== b.rows) {
            return null;
        }
        
        for (let i = 0; i < this.rows; i++) {
            result.push([]);

            for (let j = 0; j < b.columns; j++) {
                let sum = 0;

                for (let k = 0; k < this.columns; k++) {
                    sum += this._data[i][k] * b._data[k][j];
                }

                result[result.length - 1].push(sum);
            }
        }
        
        return new Matrix(result);
    }
}