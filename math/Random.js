/**
 * @class
 */
class Random {

    /**
     * @public
     * xorshift+
     */
    static random() {
        let t = this._seed[0];
        const s = this._seed[1];

        this._seed[0] = s;

        t ^= t << 23;
        t ^= t >> 17;
        t ^= s ^ (s >> 26);

        this._seed[1] = t;

        return t + s;
    }

    /**
     * @public
     */
    static setSeed(seed) {
        this._seed[0] = 1;
        this._seed[1] = seed;
    }

    /**
     * @public
     */
    static choice(iterable) {
        const arr = Array.from(iterable);
        const index = Math.floor(Math.random() * arr.length);

        return arr[index];
    }

    /**
     * @public
     */
    static range(min, max) {
        return Math.random() * (max - min) + min;
    }
}

Random._seed = [
    1,
    Math.random(),
];