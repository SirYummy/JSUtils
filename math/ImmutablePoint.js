/**
 * @class
 */
class ImmutablePoint {

    /**
     * @constructor
     */
    constructor() {
        if (Array.isArray(arguments[0])) {
            this._values = arguments[0];
        } else {
            this._values = arguments;
        }
    }

    /**
     * @public
     */
    mult(multiplier) {
        let newValues;

        if (multiplier instanceof Number) {
            newValues = this._values.map(x => x * multiplier);

        } else if (multiplier instanceof ImmutablePoint) {
            newValues = [];

            for (let i = 0; i < this._values.length; i++) {
                newValues.push(this._values[i] * increment.values[i]);
            }
        }

        return new ImmutablePoint(newValues);
    }

    /**
     * @public
     */
    add(increment) {
        let newValues;

        if (multiplier instanceof Number) {
            newValues = this._values.map(x => x + multiplier);

        } else if (multiplier instanceof ImmutablePoint) {
            newValues = [];

            for (let i = 0; i < this._values.length; i++) {
                newValues.push(this._values[i] + increment.values[i]);
            }
        }

        return new ImmutablePoint(newValues);
    }

    /**
     * @public
     */
    dist(other) {
        let total = 0.0;

        for (let i = 0; i < this._values.length; i++) {
            total += Math.pow(this._values[i] - other.values[i], 2);
        }

        return Math.sqrt(total);
    }

    /**
     * @public
     */
    get values() {
        return this._values;
    }

    /**
     * @public
     */
    get x() {
        return this._values[0];
    }

    /**
     * @public
     */
    get y() {
        return this._values[1];
    }

    /**
     * @public
     */
    get z() {
        return this._values[2];
    }
}