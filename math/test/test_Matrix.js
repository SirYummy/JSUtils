import('../Matrix.js');
import('../../util/assert.js');

const suite = describe;
const spec = it;

suite('Matrix.js', () => {
    suite('multiply()', () => {
        spec('Should have identity property', () => {
            const identity = new Matrix([
                [1, 0, 0],
                [0, 1, 0],
                [0, 0, 1],
            ]);

            const jumble = new Matrix([
                [9, 4, 3],
                [8, 5, 2],
                [7, 6, 1],
            ]);

            const result = identity.multiply(jumble);

            assertDeepEqual(result._data, jumble._data)
        });
    });
});