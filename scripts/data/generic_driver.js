import('/src/JSUtils/util/Driver.js');
import('/src/JSUtils/util/Page.js');

/**
 * @class
 */
class MyNewViewDriver extends Driver {

    /**
     * @public
     */
    static init() {
        super.init();
    }
}

Page.addLoadEvent(() => {
    MyNewViewDriver.init();
});

const ID = {
    CONTAINER: 'container',
};

const CLASS = {

};
