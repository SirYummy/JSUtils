import('/src/JSUtils/components/Component.js');

/**
 * @class
 */
class MyComponent extends Component {

    /**
     * @constructor
     */
    constructor() {
        const params = {};
        
        super(MyComponent.ID.TEMPLATE.THIS, params);
    }
}

MyComponent.ID = {
    TEMPLATE: {
        THIS: 'MyComponent_template',
    },
};
