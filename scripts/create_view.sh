#!/bin/bash

mkdir -p ./src/$1
VIEW=$2
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
TARGET=./src

cp -f $DIR/data/generic_index.html $TARGET/$1/index.html
sed -i -e 's/MyNewView/'"$VIEW"'/g' $TARGET/$1/index.html

cp -f $DIR/data/generic_driver.js $TARGET/$1/driver.js
sed -i -e 's/MyNewView/'"$VIEW"'/g' $TARGET/$1/driver.js
