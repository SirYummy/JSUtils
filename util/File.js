/**
 * @class
 */
class File {

    /**
     * @public
     * https://stackoverflow.com/questions/13405129/javascript-create-and-save-file
     */
    static save(data, filename, type) {
        let file = new Blob([data], {type: type});
        if (window.navigator.msSaveOrOpenBlob) // IE10+
            window.navigator.msSaveOrOpenBlob(file, filename);
        else { // Others
            let a = document.createElement("a"),
                    url = URL.createObjectURL(file);
            a.href = url;
            a.download = filename;
            document.body.appendChild(a);
            a.click();
            setTimeout(function() {
                document.body.removeChild(a);
                window.URL.revokeObjectURL(url);  
            }, 0); 
        }
    }

    /**
     * @public
     */
    static upload(fileInput, endpoint) {
        let formData = new FormData();

        formData.append('file', fileInput.files[0]);

        return Request.post(endpoint, formData, null, true).then(response => {
            return JSON.parse(response).fileName;
        });
    }

    /**
     * @public
     */
    static readAsText(fileInput) {
        const file = fileInput.files[0];
        const reader = new FileReader();
        
        return new Promise((resolve, reject) => {
            reader.onload = e => {
                resolve(reader.result);
            };

            reader.readAsText(file);
        });
    }
}