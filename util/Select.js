import('generic.js');
import('../components/Component.js');

/**
 * @class
 * Class for building select HTML elements.
 * @param {String} name - The name attribute for the select element.
 * @param {String[]|Object<String, String>} optEnum - An iterable with values to be used as options.
 * @param {Boolean} useValues - Whether to use the values of optEnum as values for
 * the select options. Default false (use keys).
 * @param {Boolean} useValuesForLabels - Whether to use the values of optEnum as values for
 * the option labels. Default true.
 */
class Select extends Component {
    /**
     * @constructor
     */ 
    constructor(name, optEnum, useValues, useValuesForLabels) {
        super('', {});

        this.name = name;
        this._options = [];
        this._onChange = [];
        useValues = useValues || false;
        if (useValuesForLabels === undefined) useValuesForLabels = true;

        if (optEnum) {
            for (let key in optEnum) {
                let label;
                
                if (useValuesForLabels) {
                    label = optEnum[key];
                } else {
                    label = key;
                }

                if (useValues) {
                    this.addOption(optEnum[key], label, false);
                } else {
                    this.addOption(key, label, false);
                }
            }
        }
    }

    /**
     * Adds an option.
     * @param {String} value - The value attribute.
     * @param {String} label - The the string to show for the option in the UI.
     * @param {Boolean} selected - Whether the option is selected.
     * @return {Select} - This Select.
     */
    addOption(value, label, selected) {
        const result = new Select.Option(value, label, selected);
        
        this._options.push(result);

        return this;
    }

    /**
     * Sets a single option to be selected by value.
     * @param {String} value
     * @return {Select} - This Select.
     */
    setSelected(value) {
        this._options.forEach((opt) => {
            opt.selected = String(opt.value) === String(value);
        });

        return this;
    }

    /**
     * Sets a single option to be selected by label.
     * @param {String} label
     * @return {Select} - This Select.
     */
    setSelectedByLabel(label) {
        this._options.forEach((opt) => {
            opt.selected = opt.label === label;
        });

        return this;
    }

    /**
     * @public
     */
    setOnChange(callback) {
        this._onChange.push(callback);

        return this;
    }

    /**
     * @public
     */
    getValue() {
        const element = Dom.getById(this.getId());

        return element.options[element.selectedIndex].value;
    }

    /**
     * @public
     */
    onChange() {
        const value = this.getValue();

        this._onChange.forEach(callback => {
            callback(value);
        });
    }

    /**
     * Renders the select.
     * @return {String} - The rendered HTML.
     */
    render() {
        let html = `<select id="${this.getId()}" name="${this.name}"`;

        if (this._onChange) {
            html = html + ` onchange="${this._params.this}.onChange();"`;
        }
        html = html + '>';

        this._options.forEach((opt) => {
            html = html + `<option value="${opt.value}"`;
            if (opt.selected) {
                html = html + ' selected';
            }

            html = html + '>' + opt.label + '</option>';
        });

        return html + '</select>';
    }
}

Select.Option = class {
    constructor(value, label, selected) {
        if (value === undefined) value = 'option';
        this.value = value;

        if (label === undefined) label = 'label';
        this.label = label;
        
        this.selected = selected || false;
    }
};