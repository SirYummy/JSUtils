[[QueryString.js]]

/**
 * @class
 */
class Request {

    /**
     * @public
     * @param {String} url
     * @param {Boolean} crossOrigin - Default false.
     */
    static get(url, crossOrigin) {
        if (crossOrigin === undefined) crossOrigin = false;
        
        return new Promise((resolve, reject) => {
            const request = new XMLHttpRequest();

            request.onreadystatechange = () => {
                if (request.readyState === Request.STATE.DONE) {
                    if (request.status === Request.RESPONSE.OK) {
                        const data = request.responseText;
                        resolve(data);
                    }
                    else {
                        reject(request.status);
                    }
                }
            };

            request.open('GET', url, crossOrigin);
            request.send();
        });
    }

    /**
     * @public
     * @param {String} endpoint
     * @param {Object} data
     * @param {Object} headers
     * @param {Boolean} crossOrigin - Default false.
     */
    static post(endpoint, data, headers, crossOrigin, format) {
        if (crossOrigin === undefined) crossOrigin = false;
        if (format === undefined) format = Request.DATA_FORMAT.URL_ENCODED;

        if (!(data instanceof FormData)) {
            if (format === Request.DATA_FORMAT.URL_ENCODED) {
                data = QueryString.build('', data).slice(1);
            } else if (format === Request.DATA_FORMAT.JSON) {
                data = JSON.stringify(data);
            }    
        }

        return new Promise((resolve, reject) => {
            const request = new XMLHttpRequest();

            request.onreadystatechange = () => {
                if (request.readyState === Request.STATE.DONE) {
                    if (request.status === Request.RESPONSE.OK) {
                        resolve(request.responseText);
                    }
                    else {
                        reject(request.status);
                    }
                }
            };

            request.open('POST', endpoint, crossOrigin);

            // Let the browser set this header if FormData.
            if (!(data instanceof FormData)) {
                request.setRequestHeader('content-type', format);
            }
            
            for (let header in headers) {
                request.setRequestHeader(header, headers[header]);
            }
            request.send(data);
        });
    }
}

Request.DATA_FORMAT = {
    URL_ENCODED: 'application/x-www-form-urlencoded',
    JSON: 'application/json',
    MULTIPART_FORM_DATA: 'multipart/form-data',
};

Request.STATE = {
    UNSENT: 0,
    OPENED: 1,
    HEADERS_RECEIVED: 2,
    LOADING: 3,
    DONE: 4
};

Request.RESPONSE = {
    OK: 200,
    REDIRECT: 302,
};