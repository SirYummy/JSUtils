class XML {
    /**
     * Taken from: https://davidwalsh.name/convert-xml-json (with modification).
     */
    static toJson(xml) {
        
        // Create the return object
        let obj = {};

        if (xml.nodeType == 1) { // element
            // do attributes
            if (xml.attributes.length > 0) {
            obj["_attributes"] = {};
                for (let j = 0; j < xml.attributes.length; j++) {
                    const attribute = xml.attributes.item(j);
                    obj["_attributes"][attribute.nodeName] = attribute.nodeValue;
                }
            }
        } else if (xml.nodeType == 3) { // text
            obj = xml.nodeValue;
        }

        // do children
        if (xml.hasChildNodes()) {
            for(let i = 0; i < xml.childNodes.length; i++) {
                const item = xml.childNodes.item(i);
                const nodeName = item.nodeName;
                if (nodeName === '#text') {
                    obj = item.nodeValue;
                }
                else if (typeof(obj[nodeName]) == "undefined") {
                    obj[nodeName] = this.toJson(item);
                } else {
                    if (typeof(obj[nodeName].push) == "undefined") {
                        let old = obj[nodeName];
                        obj[nodeName] = [];
                        obj[nodeName].push(old);
                    }
                    obj[nodeName].push(this.toJson(item));
                }
            }
        }
        return obj;
    };   
}
