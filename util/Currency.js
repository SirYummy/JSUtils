/**
 * @class
 */
class Currency {

    /**
     * @public
     * @param {Number} valueUSCents
     */
    static formatUSD(valueUSCents) {
        let dollars = Math.abs(valueUSCents / 100).toFixed(2);

        if (valueUSCents < 0) {
            dollars = '-$' + dollars;
        } else {
            dollars = '$' + dollars;
        }

        return dollars
    }
}