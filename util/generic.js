/**
 * Converts the contents of a function into a string.
 */
function fToString(f, values) {
    let entire = f.toString(); 

    for (let key in values) {
        if (typeof(values[key]) === 'string') {
            entire = entire.replace(key, '`' + values[key] + '`');
        } else {
            entire = entire.replace(key, values[key]);
        }
    }

    return entire.slice(entire.indexOf('{') + 1, entire.lastIndexOf('}')).trim();
}