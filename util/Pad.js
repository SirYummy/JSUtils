/**
 * @class
 */
class Pad {

    /**
     * @public
     */
    static pad(value, length, char, side) {
        side = side || Pad.SIDE.LEFT;

        const numAdditionalChars = Math.max(length - value.length, 0);
        const chars = char.repeat(numAdditionalChars);

        if (side === Pad.SIDE.LEFT) {
            return chars + value;
        }
    
        return value + chars;
    }
}

Pad.SIDE = {
    LEFT: 'left',
    RIGHT: 'right',
};