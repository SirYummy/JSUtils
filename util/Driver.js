import('../components/InfiniteScrollList.js');

/**
 * @class
 * View driver.
 */
class Driver {
    
    /**
     * @public
     * Sets up the view.
     * Typically called when a page first loads.
     */
    static init() {
        Driver._driver = Driver._driver || this;
    }

    /**
     * @public
     * Returns the first driver initialized on the given page.
     */
    static getViewDriver() {
        return Driver._driver;
    }

    /**
     * @private
     */
    static _listAndRender(containerId, loadF, component) {
        const renderF = app => {
            return new component(app).render();
        };

        return new InfiniteScrollList(
            containerId,
            null,
            renderF,
            loadF,
        ).loadMore();
    }
}

Driver._driver = null;
