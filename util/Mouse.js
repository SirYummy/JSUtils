[[Page.js]]

/**
 * @class
 */
class Mouse {

    /**
     * @public
     */
    static isDown() {
        return this._isDown;
    }

    /**
     * @public
     */
    static getPositionRelativeToElement(event, element) {
        const offset = element.getBoundingClientRect();

        return {
            x: event.pageX - offset.left,
            y: event.pageY - offset.top
        }
    }

    /**
     * @public
     */
    static getPositionOnPage(event) {
        event = event || this._event;
        
        return {
            x: event.pageX,
            y: event.pageY
        }
    }

    /**
     * @public
     */
    static onPress(callback) {
        this._onPress.push(callback);
    }

    /**
     * @public
     */
    static onRelease(callback) {
        this._onRelease.push(callback);
    }

    /**
     * @public
     */
    static onMove(callback) {
        this._onMove.push(callback);
    }

    /**
     * @public
     * @return {Number}
     * The change in the x position of the mouse since last update.
     */
    static get dx() {
        return this._x - this._px;
    }

    /**
     * @public
     * @return {Number}
     * The change in the y position of the mouse since last update.
     */
    static get dy() {
        return this._y - this._py;
    }
}

Mouse._onRelease = [];
Mouse._onPress= [];
Mouse._onMove = [];
Mouse._px = 0;
Mouse._py = 0;
Mouse._x = 0;
Mouse._y = 0;

Page.addLoadEvent(() => {
    document.body.onmousedown = () => {
        Mouse._isDown = true;
        Mouse._onPress.forEach(callback => {
            callback();
        });
    };

    document.body.onmouseup = () => {
        Mouse._isDown = false;
        Mouse._onRelease.forEach(callback => {
            callback();
        });
    };

    document.onmousemove = (event) => {
        Mouse._px = Mouse._x;
        Mouse._py = Mouse._y;

        const pos = Mouse.getPositionOnPage(event);
        Mouse._x = pos.x;
        Mouse._y = pos.y;

        Mouse._onMove.forEach(callback => {
            callback();
        });
    };
});