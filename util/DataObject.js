/**
 * An object carrying data to/from the backend.
 * @class
 */
class DataObject {

    /**
     * @public
     * @param {Object} d
     */
    constructor(d) {
        if (d) {
            Object.assign(this, d);
        }

        this.init();
    }

    copy() {
        return new this.constructor(this);
    }

    /**
     * @public
     * Builds an instance of this class using properties from an object.
     * @param {Object} d
     * @param {Boolean} propertyCheck - Whether or not to only use values for this class' instance properties.
     * @return {DataObject}
     */
    static fromDict(d, propertyCheck) {
        let result = new this();
        if (propertyCheck === undefined) propertyCheck = true;

        for (let key in d) {
            if (propertyCheck && !(key in result)) {
                console.debug('Warning: Attempt to set unkown property on ' + this.name + ' instance: ' + key);
                console.debug((new Error()).stack);
            } else {
                result[key] = d[key];
            }
        }

        result.init();

        return result;
    }

    /**
     * @public
     * @abstract
     */
    init() {

    }
}
