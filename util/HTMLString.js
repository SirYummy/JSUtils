/**
 * @class
 */
class HTMLString {
    
    /**
     * @public
     */
    static escape(s) {
        if (!s) return s;
        
        return s.replace(this._PATTERN, (match) => {
            return this._REPLACEMENTS[match];
        });
    }
}

HTMLString._PATTERN = /[&<>"'\/]/g;
HTMLString._REPLACEMENTS = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#x27;',
    '/': '&#x2F;',
};