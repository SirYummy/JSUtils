const KEYS = {
    A_KEY: ['a', 65],
    B_KEY: ['b', 66],
    C_KEY: ['c', 67],
    ARROW_LEFT: ['ArrowLeft', 37],
    ARROW_RIGHT: ['ArrowRight', 39],
    ARROW_DOWN: ['ArrowDown', 40],
    ARROW_UP: ['ArrowUp', 30],
    D_KEY: ['d', 68],
    E_KEY: ['e', 69],
    ENTER_KEY: ['Enter', 13],
    ESCAPE_KEY: ['Escape', 27],
    S_KEY: ['s', 83],
    SPACE: [' ', 32],
    W_KEY: ['w', 87],
};

/**
 * @class
 */
class Key {

    /**
     * @public
     */
    static on(key, callback) {
        return event => {
            const eventKey = event.key || event.which || event.keyCode;

            if (this.equals(eventKey, key)) {
                callback(event);
            }
        };
    }

    /**
     * @public
     */
    static onKeyReleased(key, callback) {
        key = Key._uniformKeys[key[0]];

        if (!Key._keyReleaseCallbacks[key]) {
            Key._keyReleaseCallbacks[key] = [];
        }

        Key._keyReleaseCallbacks[key].push(callback);
    }

    /**
     * @public
     */
    static onKeyPressed(key, callback) {
        key = Key._uniformKeys[key[0]];

        if (!Key._keyPressCallbacks[key]) {
            Key._keyPressCallbacks[key] = [];
        }

        Key._keyPressCallbacks[key].push(callback);
    }

    /**
     * @public
     */
    static equals(key1, key2) {
        return key2.indexOf(key1) > -1;
    }

    /**
     * @public
     * @return {Boolean}
     */
    static isDown(key) {
        key = Key._uniformKeys[key[0]];

        return Boolean(Key._keysDown[key]);
    }

    /**
     * @public
     */
    static get(event) {
        let eventKey = event.key || event.which || event.keyCode;

        return Key._uniformKeys[eventKey];
    }
}

Key._keysDown = {};
Key._uniformKeys = {};
Key._keyReleaseCallbacks = {};
Key._keyPressCallbacks = {};

Object.keys(KEYS).forEach(key => {
    const keyValue = KEYS[key];

    keyValue.forEach(value => {
        Key._uniformKeys[value] = key;
    });
});

document.onkeydown = function(e) {
    const key = Key.get(e);

    Key._keysDown[key] = true;

    if (Key._keyPressCallbacks[key]) {
        Key._keyPressCallbacks[key].forEach(callback => {
            callback(key);
        });
    }
};

document.onkeyup = function(e) {
    const key = Key.get(e);

    Key._keysDown[key] = false;

    if (Key._keyReleaseCallbacks[key]) {
        Key._keyReleaseCallbacks[key].forEach(callback => {
            callback(key);
        });
    }
};
