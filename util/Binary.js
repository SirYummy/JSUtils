/**
 * @class
 */
class Binary {

    /**
     * @public
     */
    static asUnsignedInt(bits) {
        let value = 0;

        for (let i = 0; i < bits.length; i++) {
            const bit = bits[bits.length - 1 - i];
            const placeValue = Math.pow(2, i);

            value += Number(bit) * placeValue;
        }

        return value;
    }
}