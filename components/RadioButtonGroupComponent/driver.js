import('/src/JSUtils/components/Component.js');

/**
 * @class
 */
class RadioButtonGroupComponent extends Component {

    /**
     * @constructor
     */
    constructor(values, labels) {
        const params = {};
        
        super(RadioButtonGroupComponent.ID.TEMPLATE.THIS, params);
    
        this._values = values;
        this._labels = labels;
        this._i = 0;
        this._buttonComponents = [];
        this._onSelectedCallbacks = [];

        for (let i = 0; i < values.length; i++) {
            const button = new ButtonComponent(labels[i])
                .setOnClick(() => {
                    this.setSelected(i);
                });

            this._buttonComponents.push(button);
        }
    }

    /**
     * @public
     */
    setButtonClassNames(classNames) {
        this._buttonComponents.forEach(button => {
            button.setClassNames(classNames);
        });

        return this;
    }

    /**
     * @public
     */
    render() {
        let buttons = '';

        this._buttonComponents.forEach(button => {
            buttons = buttons + button.render();
        });

        this._params.buttons = buttons;

        return super.render();
    }

    /**
     * @public
     */
    setOnSelected(callback) {
        this._onSelectedCallbacks.push(callback);

        return this;
    }

    /**
     * @public
     */
    setSelected(i) {
        this._i = i;

        for (let j = 0; j < this._buttonComponents.length; j++) {
            let button = this._buttonComponents[j];

            button.setDisabled(j === i);
        }

        this._onSelectedCallbacks.forEach(callback => {
            callback(this._values[i]);
        });

        return this;
    }

    /**
     * @public
     */
    getSelected() {
        return this._values[this._i];

        return this;
    }
}

RadioButtonGroupComponent.ID = {
    TEMPLATE: {
        THIS: 'RadioButtonGroupComponent_template',
    },
};
