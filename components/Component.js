import('../util/generic.js');
import('../util/JSRender.js');

/**
 * A UI component based on a given template and template parameters.
 * Injects special values into the given template:
 * componentId - ID of the component.
 * @class
 * this - A function call which returns component itself.
 * @param {String} templateId - ID of the JSRender template for this component.
 * @param {Object} params - Template params.
 */
class Component {

    /**
     * @constructor
     */
    constructor(templateId, params) {
        this._template = templateId;
        this._params = Object.assign({}, params);
        this._id = this.constructor._generateId();
        this._params.componentId = this._id;
        this._params.this = 'Component.get(\'' + this._id + '\')';
        this._onRender = [];
        this._renderPollInterval = null;
        this._finishedRenderingPromise = new Promise((resolve, reject) => {
            this._finishedRenderingResolver = resolve;
        });
        
        Component._components[this._id] = this;
    }

    /**
     * Sets additional CSS class names to be rendered into the template.
     * Sets this._params.classNames.
     * @public
     * @param {String} classNames
     * @return {PackageGroupFormComponent}
     */
    setClassNames(classNames) {
        this._params.classNames = classNames;

        return this;
    }

    /**
     * Returns this ID of this Component.
     * @return {String}
     */
    getId() {
        return this._id;
    }

    /**
     * Returns a sub-element of this component by ID.
     * @public
     * @param {String} id
     * @return {Element}
     */
    getElement(id) {
        return Dom.getById(this.getId() + id);
    }

    /**
     * Returns this.constructor.ID.
     * @public
     */
    _ids() {
        return this.constructor.ID;
    }

    /**
     * Returns the rendered component.
     * @return {String} - The rendered HTML.
     */
    render() {     
        let result = JSRender.render(this._template, this._params);

        if (this._onRender) {
            this._renderPollInterval = setInterval(() => {
                this._checkHasRendered();
            }, 50);
        }

        return result;
    }

    /**
     * Called before the component is re-rendered in the UI with update().
     * @abstract
     */
    _initParams() {

    }

    /**
     * Re-initializes template params and re-renders the component in the UI.
     * @public
     */
     update() {
        this._initParams();

        if (this.isRendered()) {
            Dom.replaceWith(this.getId(), this.render());
        }
     }

    /**
     * Sets a function to be called when the component is rendered in the DOM.
     * NOTE: The top-level element in the component must have id="[[componentId]]".
     * @public
     */
    setOnRender(f) {
        this._onRender.push(f);
    }

    /**
     * Removes this component from the DOM.
     * @public
     */
    delete() {
        Dom.removeElement(this.getId());
    }

    /**
     * @public
     * @return {Boolean}
     */
    isRendered() {
        const element = document.getElementById(this._id);

        return Boolean(element);
    }

    /**
     * @public
     * @returns {Promise}
     * Returns a Promise which resolves when the component is rendered in the UI.
     */
    finishRendering() {
        return this._finishedRenderingPromise;
    }

    /**
     * @private
     */
    _checkHasRendered() {
        if (this.isRendered()) {
            clearInterval(this._renderPollInterval);

            this._finishedRenderingResolver();

            this._onRender.forEach(callback => {
                callback();
            });
        }
    }


    /**
     * Returns a Component by ID.
     * @param {String} id
     * @return {Component}
     */
    static get(id) {
        return this._components[id];
    }

    /**
     * @private
     * Returns a unique Component ID.
     * @return {String}
     */
    static _generateId() {
        const result = this.name + '_' + this._nextId;

        this._nextId++;

        return result;
    }
}

Component._components = {};
Component._nextId = 0;