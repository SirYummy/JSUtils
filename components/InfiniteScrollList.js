import('../util/Dom.js');

/**
 * Controller for incremental-loading lists.
 * @class
 * @param {String} containerId - Id of the scroll container for the list.
 * @param {?String} renderId - Id of the element to render the list elements inside of.
 * @param {Function} renderFunction
 * @param {Function} loadFunction - f(offset, limit)
 */
class InfiniteScrollList {

    /**
     * @constructor
     */
    constructor(containerId, renderId, renderFunction, loadFunction) {
        this._container = Dom.getById(containerId);
        this._renderElement = Dom.getById(renderId || containerId);
        this._render = renderFunction;
        this._load = loadFunction;
        this._data = [];
        this._elements = [];
        this._threshold = 500;
        this._startIndex = 0;
        this._N = 16;
        this._stopped = false;
        this._loading = false;
        this._moreData = true;
        this._onFirstLoad = null;
        this._filterF = null;
        this._loadingIcon = null;
        this._loadingIconId = null;
        this._noResultsMessage = 'No results';
        this._loadingAll = false;
        this._finishedLoadingResolver = null;

        /**
         * Prevents promises from interfering with each other after a reset.
         * @private
         * @type {number}
         */
        this._version = 0;

        this._addScrollEvent();
    }

    /**
     * @public
     * @param {String} message
     * @return {InfiniteScrollList} - this
     */
    setNoResultsMessage(message) {
        this._noResultsMessage = message;

        return this;
    }

    /**
     * Specifies the HTML to use as a loading icon.
     * @param {String} html
     * @return {InfiniteScrollList}
     */
    setLoadingIcon(html) {
        this._loadingIcon = html;

        return this;
    }

    /**
     * Filters rendered items by a given function.
     * @param {Function} f
     */
    setFilter(f) {
        this._filterF = f;

        return this;
    }

    /**
     * Specifies a function to be called when the first data is loaded.
     * @param {Function} f
     */
    setOnFirstLoad(f) {
        this._onFirstLoad = f;

        return this;
    }

    /**
     * Sets a function to be used to load data
     * @param {Function} f - A function of the form: f(offset, limit): object[]
     */
    setLoadFunction(f) {
        this._load = f;

        return this;
    }

    /**
     * Resets the list back to load form the 0th item..
     */
    reset() {
        this._startIndex = 0;
        this._moreData = true;
        this._loading = false;
        this._version++;

        Dom.setContents(this._renderElement, '');
        this.loadMore();
    }

    /**
     * @private
     */
    _getMaxScroll() {
        return this._container.scrollHeight - this._container.clientHeight;
    }

    /**
     * @private
     */
    _onScroll(e) {
        if (this._stopped) return;

        if (
            this._loadingAll
            || !this._hasVisibleScrollbar()
            || this._container.scrollTop > this._getMaxScroll() * 0.9
        ) {
            this.loadMore();
        }
    }

    /**
     * @private
     */
    _hasVisibleScrollbar() {
        return this._container.scrollHeight > this._container.clientHeight;
    }

    /**
     * @private
     */
    _addScrollEvent() {
        this._container.addEventListener('scroll', (e) => {
            return this._onScroll(e);
        });
    }

    /**
     * @private
     */
    _renderDatum(datum) {
        if (this._stopped) return;

        const html = this._render(datum);
        this._renderElement.insertAdjacentHTML('beforeend', html);
    }

    /** 
     * @private
     */
    _append(datum) {
        let element;

        this._renderDatum(datum);
        element = this._renderElement.lastChild;
        this._data.push(datum);
        this._elements.push(element);
    }

    /**
     * @private
     */
    _showNoResultsMessage() {
        let div = document.createElement('div');
        Dom.setContents(div, this._noResultsMessage);
        
        Dom.setContents(this._renderElement, div);
    }

    /**
     * Returns a list of elements rendered into the UI so far.
     * @return {Element[]}
     */
    getListElements() {
        return this._elements;
    }

    /**
     * Returns true if more data can be loaded.
     * @return {Boolean}
     */
    canLoadMore() {
        return this._moreData;
    }

    /**
     * Stops loading data.
     * @return {InfiniteScrollList}
     */
    stop() {
        this._moreData = false;
        this._stopped = true;

        return this;
    }

    /**
     * @private
     */
    static _generateId() {
        this._lastId++;

        return this.name + '_' + this._lastId;
    }

    /**
     * @private
     */
    _showLoadingIcon() {
        if (!this._loadingIcon) return;
        
        let element = Dom.htmlToElement(this._loadingIcon);

        element.id = InfiniteScrollList._generateId();
        this._loadingIconId = element.id;
        Dom.appendTo(this._renderElement, Dom.elementToHTML(element));
    }

    /**
     * @private
     */
    _removeLoadingIcon() {
        try {
            Dom.removeElement(this._loadingIconId);
        } catch (e) {
            console.debug(e);
        }

        this._loadingIconId = null;
    }

    /**
     * Loads more data. Begins the loading process.
     * @return {InfiniteScrollList}
     */
    loadMore() {
        if (!this.canLoadMore() || this._loading) return this;

        this._loading = true;

        this._showLoadingIcon();

        const version = this._version;

        this._load(this._startIndex, this._N).then(response => {
            if (version !== this._version) return;

            const numResponses = response.length;

            // Handle final data.
            if (response.length < this._N) {
                this._moreData = false;
                this._removeLoadingIcon();

                if (this._finishedLoadingResolver) {
                    this._finishedLoadingResolver();
                }
            }

            if (this._filterF) response = Array.from(response.filter(this._filterF));

            // Handle initial data.
            if (this._startIndex === 0) {
                if (this._onFirstLoad) this._onFirstLoad();
                if (response.length === 0) this._showNoResultsMessage();
            }

            response.forEach((datum) => {
                this._append(datum);
            });
            this._loading = false;
            this._startIndex += numResponses;

            this._onScroll();
        }).catch(err => {
            console.error(err);
            this._renderElement.insertAdjacentHTML('beforeend',
                '<div>Something went wrong. Please try again later.</div>');
        });

        return this;
    }

    /**
     * @public
     * @returns {Promise}
     */
    loadAll() {
        if (!this._moreData) return Promise.resolve();

        this._loadingAll = true;
    }

    /**
     * Clears the list and re-renders everything that has already been loaded.
     * @public
     */
     reRenderContent() {
        Dom.setContents(this._renderElement, '');

        let data = this._data;

        if (this._filterF) data = data.filter(this._filterF);

        data.forEach(datum => {
            this._renderDatum(datum);
        });
     }
}

InfiniteScrollList._lastId = -1;