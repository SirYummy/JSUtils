import('../Component.js');

/**
 * @name SearchSuggestionComponent.queryResultLookupF
 * @function
 * @param {String} query - The query.
 * @return {Promise<Component[]>} - The query results
 */

/**
 * @name SearchSuggestionComponent.queryResultRenderF
 * @function
 * @param {Object} queryResult - An object return in the query results.
 * @return {Component} - A component
 */

/**
 * @class
 * @param {?String} templateId - ID of the template to render.
 * @param {Object<String, Object>} params - Template rendering params and values.
 * @param {SearchSuggestionComponent.queryResultLookupF} getQueryResultsF - Function to return results given a query.
 * @param {SearchSuggestionComponent.queryResultRenderF} renderResultF - Function to render a given query result.
 * @param {?String} containerId - ID suffix of the container into which to render results. (Will be passed to this.getElement())
 * @param {?String} queryInputId - ID suffix of the query input element. (Will be passed to this.getElement())
 */
class SearchSuggestionComponent extends Component {

    /**
     * @constructor
     */
    constructor(templateId, params, getQueryResultsF, renderResultF, queryInputId, containerId) {
        templateId = templateId || 'SearchSuggestionSelectorComponent_template';
        
        super(templateId, params);
    
        this._queryTimeoutInterval = null;
        this._selectedObject = null;
        this._getQueryResultsF = getQueryResultsF;
        this._renderResultF = renderResultF;
        this._containerId = containerId || '_resultsContainer';
        this._queryInputId = queryInputId || '_queryInput';
        this._elementSelectedCallbacks = [];
        this._onResultsLoaded = [];
        this._objectsById = {};
        this._onClearedCallbacks = [];

        this.setOnRender(() => {
            let input = this.getElement(this._queryInputId);
            let results = this._getResultsContainer();

            input.addEventListener('keyup', event => {
                const query = event.target.value;

                this.onChange(query);
            });

            input.addEventListener('click', event => {
                this.onClick();
            });

            results.addEventListener('click', event => {
                let element = event.target;

                if (event.target === results) return;

                // Get top element which is child of container.
                while (element.parentElement && element.parentElement !== results) {
                    element = element.parentElement;
                }

                this._onElementSelected(element);
            });
        });
    }

    /**
     * @public
     * @param {Object} obj
     */
    setSelected(obj, callbacks) {
        if (!obj) return this;
        if (callbacks === undefined) callbacks = true;
        
        this._onElementSelected(obj, callbacks);

        return this;
    }

    /**
     * Sets up a callback to execute when a result element is selected.
     * @public
     * @return {SearchSuggestionComponent} - this
     */
    onElementSelected(callback) {
        this._elementSelectedCallbacks.push(callback);

        return this;
    }

    /**
     * @public
     */
    setLabel(label) {
        this._params.label = label;

        try {
            this.getElement(this._queryInputId).value = label;
        } catch (e) {
            // Not rendered.
        }

        return this;
    }

    /**
     * @private
     */
    _onElementSelected(obj) {
        this._clearResults();
        
        if (obj instanceof Element) {
            this._selectedObject = this._objectsById[obj.id];
        } else {
            this._selectedObject = obj;
        }
        
        this._elementSelectedCallbacks.forEach(callback => {
            callback(this._selectedObject);
        });
    }

    /**
     * @public
     */
    onChange(query) {
        if (this._queryTimeoutInterval) {
            clearInterval(this._queryTimeoutInterval);
        }

        this._queryTimeoutInterval = setTimeout(() => {
            this._query(query);
        }, this.constructor._QUERY_TIMEOUT_MS);
    }

    /**
     * @public
     */
    onResultsLoaded(callback) {
        this._onResultsLoaded.push(callback);
    }

    /**
     * @private
     */
    _query(query) {
        let container = this._getResultsContainer();

        Dom.setContents(container, '');

        if (!query) return;

        this._getQueryResultsF(query).then(objects => {
            if (!objects.length) {
                Dom.setContents(container, 'No Results');
            }

            objects.forEach(object => {
                let rendered = this._renderResultF(object);

                if (rendered instanceof Component) {
                    this._objectsById[rendered.getId()] = object;

                    rendered = rendered.render();
                }

                Dom.appendTo(container, rendered);
            });

            this._onResultsLoaded.forEach(callback => {
                callback();
            });
        });
    }

    /**
     * @private
     */
    _getResultsContainer() {
        return this.getElement(this._containerId);
    }

    /**
     * @public
     */
    onClick() {
        this.clear();
    }

    /**
     * @private
     */
    _clearResults() {
        try {
            let results = this._getResultsContainer();
            Dom.setContents(results, '');
        } catch (e) {
            // Not yet rendered.
        }
    }

    /**
     * @public
     */
    clear(callbacks) {
        if (callbacks === undefined) callbacks = true;

        try {
            let queryInput = this.getElement(this._queryInputId);
            this._clearResults();

            queryInput.value = '';
            this._selectedObject = null;

            if (callbacks) {
                this._onClearedCallbacks.forEach(callback => {
                    callback();
                });
            }
        } catch (e) {
            // Not rendered.
        }
    }

    /**
     * @private
     */
    setOnCleared(callback) {
        this._onClearedCallbacks.push(callback);
    }

    /**
     * @public
     * @return {Object}
     */
    getSelected() {
        return this._selectedObject;
    }
}

SearchSuggestionComponent._QUERY_TIMEOUT_MS = 500;
