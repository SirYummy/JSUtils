import('../util/generic.js');

/**
 * @class
 * TODO: Return Element instead of HTML.
 */
class Table {
    /**
     * @constructor
     */
    constructor() {
        this._columns = [];
        this._classNames = null;
        this._id = null;
        this._onReorder = null;
        this._subtable = null;
        this._subtableGetterF = null;
        this._orderByColumnName = {};
    }

    /**
     * @public
     */
    copy() {
        let result = new Table();

        result._columns = this._columns;
        result._classNames = this._classNames;
        result._onReorder = this._onReorder;
        result._subtable = this._subtable;
        result._subtableGetterF = this._subtableGetterF;

        return result;
    }

    /**
     * @public
     * @param {String} id
     */
    setId(id) {
        this._id = id;

        Table._tablesById[id] = this;

        return this;
    }

    /**
     * @public
     * @param {Function} f
     */
    setOnReorder(f) {
        this._onReorder = f;

        return this;
    }

    /**
     * Sets CSS class names with which to render each row.
     * @param {String} classNames
     * @return {Table} - This table.
     */
    setRowClassNames(classNames) {
        this._classNames = classNames;

        return this;
    }

    /**
     * @public
     * Adds a column to the table.
     *
     * @param {String} columnName
     * @param {Function} dataGetter
     * @param {?Boolean} sortable
     * @param {?String} align
     * @param {?Number} position
     *
     * @return {Table} - This table.
     */
    addColumn(columnName, dataGetter, sortable, align, colName, position) {
        if (position === undefined) position = this._columns.length;

        this._columns.splice(position, 0, new Table.Column(
            columnName, dataGetter, sortable, align, colName));

        return this;
    }

    /**
     * @public
     * @param {String} columnName
     * @return {Table}
     */
    removeColumn(columnName) {
        this._columns = this._columns.filter(col => {
            return col.name !== columnName;
        });

        return this;
    }

    /**
     * @public
     */
    getColumns() {
        return this._columns.slice();
    }

    /**
     * Returns a single rendered row given some data.
     * @param {Object} row
     * @return {String} - The rendered HTML.
     */
    renderRow(row) {
        let result = '';

        result = result + '<tr ';
        if (this._classNames) {
            result = result + 'class="' + this._classNames + '"';
           }
        result = result + '>';

        this._columns.forEach((col) => {
           result = result + '<td align="' + col.align + '"';
           // result = result + 'data-th="' + col.name + '"';

           result = result + '>' + col.getValue(row); + '</td>';
        });

        return result + '</tr>';
    }

    /**
     * @public
     */
    static getOrder(tableId, defaultOrder) {
        const element = Dom.getById(tableId);
        const table = Table._tablesById[tableId];
        let result = '';

        // TODO: Ordering.
        Object.keys(table._orderByColumnName).forEach(key => {
            const columnName = table.getColumnDatabaseName(key);
            const order = table._orderByColumnName[key];

            if (columnName && order)
                result = result + `${columnName} ${order},`;
        });

        result = result.slice(0, -1);

        if (!result || result.length === 0) {
            result = defaultOrder;
        }

        return result;
    }

    /**
     * @public
     */
    resetOrder() {
        this._orderByColumnName = {};
    }

    /**
     * @public
     */
    static _toggleSort(element, tableId) {
        if (!element.hasAttribute('data-sort')) {
            element.setAttribute('data-sort', 'ASC');
        }
        else {
            const sort = element.getAttribute('data-sort');
            if (sort === 'ASC') {
                element.setAttribute('data-sort', 'DESC');
            }
            else if (sort === 'DESC') {
                element.setAttribute('data-sort', '');
            }
            else {
                element.setAttribute('data-sort', 'ASC');
            }
        }

        if (tableId) {
            const table = Table._tablesById[tableId];
            table.setOrder(element.textContent, element.getAttribute('data-sort'));
            table._onReorder();
        }
    }

    /**
     * @public
     */
    setOrder(columnName, order) {
        this._columns
        this._orderByColumnName[columnName] = order;
        
        this._columns.forEach(col => {
            if (col.name === columnName) {
                col.order = order;
                return;
            }
        });
    }

    /**
     * @public
     */
    getColumnDatabaseName(columnName) {
        const col = this._columns.find(col => col.name === columnName);

        if (!col) return null;

        return col.colName;
    }

    /**
     * @public
     */
    renderHeader() {
        let result = document.createElement('tr');

        this._columns.forEach((col) => {
            let header = document.createElement('th');
            if (col.sortable) {
                header.setAttribute('onclick', fToString(() => {
                    Table._toggleSort(event.target, _id);
                }, {_id: this._id}));
            }
            header.align = col.align;
            header.innerHTML = col.name;
            if (col.order) {
                header.setAttribute('data-sort', col.order);
            }
            if (col._headerClass) {
                header.className = col._headerClass;
            }
            result.appendChild(header);
        });

        let container = document.createElement('div');
        container.appendChild(result);

        return container.innerHTML;
    }

    /**
     * Returns a rendered table given some data.
     * @param {Object[]} rows - A list of objects to render.
     * @return {String} - The rendered HTML.
     */
    render(rows) {
        let result = document.createElement('table');

        // Render rows.
        let body = '';
        rows.forEach((row) => {
            body = body + this.renderRow(row);
        });

        if (this._id) {
            result.setAttribute('id', this._id);
        }

        result.innerHTML = '<tbody>' + this.renderHeader() + body + '</tbody>';

        return result;
    }
}

Table._tablesById = {};

/**
 * @class
 */
Table.Column = class {
    constructor(name, dataGetter, sortable, align, colName) {
        this.name = name;
        this.getValue = dataGetter;
        this.sortable = sortable || false;
        this.align = align || 'left';
        this.order = '';
        this.colName = colName
        if (!this.colName) {
            this.colName = this.name;
        }
        this._headerClass = null;
    }

    setHeaderClass(classNames) {
        this._headerClass = classNames;
    }
};