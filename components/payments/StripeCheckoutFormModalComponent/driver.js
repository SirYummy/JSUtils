import('/src/JSUtils/util/Currency.js');

/**
 * @class
 * A modal component for checkout with Stripe.
 * Uses environment variables: stripePublishableKey, supportEmail
 * @param {Object[]} products
 * @param {Function} getChargesF
 * @param {Function} purchaseF
 * @param {String} purchasedProductsURI
 * {@link https://stripe.com/docs/stripe-js/reference}
 * {@link https://stripe.com/docs/stripe-js/elements/quickstart}
 * {@link https://stripe.com/docs/testing#cards}
 */
class StripeCheckoutFormModalComponent extends ModalComponent {

    /**
     * @constructor
     */
    constructor(products, getChargesF, purchaseF, purchasedProductsURI) {
        super();
    
        /**
         * @private
         * @type {Product[]}
         */
        this._products = products;

        this._stripe = Stripe('[["stripePublishableKey"]]');
        this._getChargesF = getChargesF;
        this._purchaseF = purchaseF;
        this._purchasedProductsURI = purchasedProductsURI;
        this._card = null;

        this.setOnRender(() => {
            this._initStripe();
        });
    }

    /**
     * @public
     * @override
     */
    render() {
        this._getChargesF(this._products).then(charges => {
            const params = Object.assign({
                charges: this._renderCharges(charges.items),
                transactionFee: Currency.formatUSD(charges.transactionFeeUSCents),
                total: Currency.formatUSD(charges.totalUSCents),
            }, this._params);

            const content = JSRender.render(this.constructor.ID.TEMPLATE.THIS, params);

            this.setContent(content);

            const rendered = super.render();

            Dom.appendTo(document.body, rendered);
        });
    }

    /**
     * @private
     */
    _renderCharges(items) {
        let result = '';

        items.forEach(item => {
            const price = Currency.formatUSD(item.priceUSCents);
            result = result + `<div>${item.name}: ${price}</div>`;
        });

        return result;
    }

    /**
     * @private
     */
    _initStripe() {
        let elements = this._stripe.elements();
        const style = {
          base: {
            // Add your base input styles here. For example:
            fontSize: '16px',
            color: "#32325d",
          }
        };

        this._card = elements.create('card', {style});
        this._card.mount('#' + this.getId() + this.constructor.ID.CARD);
    }

    /**
     * @public
     */
    onSubmitPaymentClicked() {
        Authentication.isLoggedIn();
        let paymentButton = Dom.getById(this.getId() + this.constructor.ID.PAYMENT_BUTTON);

        paymentButton.disabled = true;
        Dom.setContents(paymentButton, 'Processing, please wait...');

        this._stripe.createToken(this._card).then(result => {
            if (result.error) {
                // Inform the customer that there was an error.
                this._showErrorMessage(result.error.message);
                Dom.setContents(paymentButton, 'Error!');

                // Let them try again after a timeout.
                setTimeout(() => {
                    Dom.setContents(paymentButton, 'Submit Payment');
                    paymentButton.disabled = false;
                }, StripeCheckoutFormModalComponent._ERROR_TIMEOUT_MS);
            } else {
                this._submitPayment(result.token);
            }
        });
    }

    /**
     * @private
     */
    _showErrorMessage(message) {
        const errorId = this.getId() + this.constructor.ID.ERRORS;
        const errorElement = document.getElementById(errorId);

        errorElement.textContent = message;
    }

    /**
     * @private
     */
    _submitPayment(token) {
        this._purchaseF(this._products, token).then(result => {
            window.location = this._purchasedProductsURI;
        }).catch(err => {
            console.error(err);

            this._showErrorMessage(this.constructor._ERROR_MESSAGE);
        });
    }
}

StripeCheckoutFormModalComponent.ID = {
    CARD: '_card',
    ERRORS: '_errorsContainer',
    PAYMENT_BUTTON: '_paymentButton',
    TEMPLATE: {
        THIS: 'StripeCheckoutFormModalComponent_template',
    },
};

StripeCheckoutFormModalComponent._ERROR_MESSAGE = 'Something went wrong. Please contact us at [["supportEmail"]] for support.';
StripeCheckoutFormModalComponent._ERROR_TIMEOUT_MS = 3000;