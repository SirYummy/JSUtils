import('../util/generic.js');

/**
 * A toggle-able UI element with an arbitrary number of states.
 * @class
 * @param {dict<string, string>} labelsByState - Mapping from visible lables to state values.
 * @param {?String} id
 * @param {?String} classNames
 */
class Toggleable {

    /**
     * @constructor
     */
    constructor(labelsByState, id, classNames) {
        this._labelsByState = labelsByState;
        this._id = id || 'Toggleable_' + (new Date()).getTime() + '_' + Math.random();
        this._states = Object.keys(labelsByState);
        this._currentState = 0;
        this._classNames = classNames || '';
        this._onToggle = null;
        Toggleable._instancesById[this._id] = this;
    }

    /**
     * @public
     */
    getLabel() {
        return this._labelsByState[this._states[this._currentState]];
    }

    /**
     * @public
     */
    render() {
        let element = document.createElement('button');
        const state = this._states[this._currentState];

        element.setAttribute('onclick', fToString(() => {
            Toggleable.toggle(_id);
        }, {_id: this._id}));

        element.innerHTML = this._labelsByState[state];
        element.setAttribute('data-state', state);
        element.setAttribute('id', this._id);
        element.className = this._classNames;

        return element;
    }

    /**
     * @public
     */
    static getToggleStates() {
        let result = {};

        Object.values(this._instancesById).forEach(toggle => {
            result[toggle._id] = toggle._states[toggle._currentState];
        });

        return result;
    }

    /**
     * @public
     */
    static toggle(toggleableId) {
        const toggleable = Toggleable._instancesById[toggleableId];
        toggleable.toggle();

        const group = Toggleable.getRadioGroup(toggleable);
        group.forEach(groupie => {
            if (groupie !== toggleable) {
                groupie.reset();
            }
        });

        if (Toggleable._onToggle) {
            Toggleable._onToggle();
        }
    }

    /**
     * @public
     */
    static setOnToggle(f) {
        Toggleable._onToggle = f;
    }

    /**
     * Specifies a callback to call when toggled.
     * @public
     * @param {Function} f - f(state)
     * @return {Toggleable}
     */
    setOnToggle(f) {
        this._onToggle = f;

        return this;
    }

    static createRadioGroup(toggleables) {
        this._radioGroups.push(new Set(toggleables));
    }

    static getRadioGroup(toggleable) {
        let result = [];

        this._radioGroups.forEach(group => {
            if (group.has(toggleable)) {
                result = Array.from(group);
            }
        });

        return result;
    }

    _updateState() {
        const element = document.getElementById(this._id);
        const state = this._states[this._currentState];

        if (element) {
            element.innerHTML = this._labelsByState[state];
            element.setAttribute('data-state', state);
        }
    }

    /**
     * @public
     */
    setState(state) {
        this._currentState = this._states.indexOf(state);
        this._updateState();

        return this;
    }

    /**
     * @public
     */
     getState() {
        return this._states[this._currentState];
     }

    /**
     * @public
     */
    toggle() {

        this._currentState++;
        if (this._currentState >= this._states.length) {
            this._currentState = 0;
        }

        this._updateState();

        this._onToggle(this.getState());
    }

    /**
     * @public
     */
    reset() {
        this._currentState = 0;
        this._updateState();
    }
}

Toggleable._instancesById = {};
Toggleable._onToggle = null;
Toggleable._radioGroups = [];