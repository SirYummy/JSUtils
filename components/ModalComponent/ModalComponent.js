import('../Component.js');

/**
 * @class
 * Generic modal window.
 * @param {String} templateId
 * @param {Object} params
 */
class ModalComponent extends Component {

    /**
     * @constructor
     */
    constructor(templateId, params) {
        super(ModalComponent.TEMPLATE_ID);

        this._params.id = this.getId();
        this._params.componentId = this._params.id;
        this._onClose = null;

        if (templateId && params) {
            this._setInnerTemplate(templateId, params);
        }
    }

    /**
     * @private
     */
    _setInnerTemplate(templateId, params) {
        params = Object.assign({}, this._params, params);
        
        this.setContent(JSRender.render(templateId, params));
    }

    /**
     * @param {String|Element} content
     * @return {ModalComponent} - this
     */
    setContent(content) {
        if (typeof(content) !== 'string') content = Dom.elementToHTML(content);

        this._params.content = content;

        return this;
    }

    /**
     * @public
     */
    setOnClose(f) {
        this._onClose = f;

        return this;
    }

    /**
     * Removes this element from the UI.
     * @public
     */
    close() {
        if (this._onClose) this._onClose();

        Dom.removeElement(this.getId());
    }
}

ModalComponent.TEMPLATE_ID = 'modalComponentTemplate';