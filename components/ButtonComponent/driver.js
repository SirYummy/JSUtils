import('/src/JSUtils/components/Component.js');

/**
 * @class
 */
class ButtonComponent extends Component {

    /**
     * @constructor
     */
    constructor(label) {
        const params = {label};
        
        super(ButtonComponent.ID.TEMPLATE.THIS, params);
        
        this._onClickCallbacks = [];
    }

    /**
     * @public
     */
    onClick() {
        this._onClickCallbacks.forEach(callback => {
            callback();
        });
    }

    /**
     * @public
     */
    setOnClick(callback) {
        this._onClickCallbacks.push(callback);

        return this;
    }

    /**
     * @public
     */
    setDisabled(value) {
        this._params.disabled = value;

        this.update();

        return this;
    }
}

ButtonComponent.ID = {
    TEMPLATE: {
        THIS: 'ButtonComponent_template',
    },
};
