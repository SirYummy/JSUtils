import('../Component.js');
import('../../util/Dom.js');

/**
 * @class
 */
class NavbarComponent extends Component {

    /**
     * @constructor
     */
    constructor(pageTitles, pageURLs) {
        let links = '';

        for (let i=0; i < pageTitles.length; i++) {
            let a = document.createElement('a');
            
            a.href = pageURLs[i];
            a.innerHTML = pageTitles[i];

            links = links + Dom.elementToHTML(a);           
        }

        super(NavbarComponent.ID.TEMPLATE.THIS, {
            links,
        });
    }
}

NavbarComponent.ID = {
    TEMPLATE: {
        THIS: 'NavbarComponent_template',
    },
};