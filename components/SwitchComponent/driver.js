import('../Component.js');

/**
 * @class
 */
class SwitchComponent extends Component {

    /**
     * @constructor
     */
    constructor(name, checked) {
        super(SwitchComponent.ID.TEMPLATE.THIS, {
            name,
            checked: Boolean(checked),
            attributes: '',
        });
    }

    /**
     * @public
     */
    setAttribute(name, value) {
        this._params.attributes = this._params.attributes + ` ${name}="${value}" `;

        return this;
    }
}

SwitchComponent.ID = {
    TEMPLATE: {
        THIS: 'SwitchComponent_template',
    }
};
