import('ServiceWorker.js');

/**
 * @class
 */
class PushNotification {
    
    /**
     * @private
     */
    static _trySendNotification(message) {
        if (this.isEnabled()) {
            new Notification(message);
        } else {
            console.warn('Notifications blocked.');
        }
    }

    /**
     * @private
     * https://developers.google.com/web/fundamentals/codelabs/push-notifications/
     */
    static _urlB64ToUint8Array(base64String) {
        const padding = '='.repeat((4 - base64String.length % 4) % 4);
        const base64 = (base64String + padding)
            .replace(/\-/g, '+')
            .replace(/_/g, '/');

        const rawData = window.atob(base64);
        const outputArray = new Uint8Array(rawData.length);

        for (let i = 0; i < rawData.length; ++i) {
            outputArray[i] = rawData.charCodeAt(i);
        }
     
        return outputArray;
    }

    /**
     * @public
     * @return {Promise<Boolean>}
     */
    static requestPermission() {
        return new Promise((resolve, reject) => {
            Notification.requestPermission(permission => {
                resolve(permission);
            });
        });
    }

    /**
     * @public
     */
    static subscribe(publicKey) {
        const registration = ServiceWorker.getRegistration();

        if (!registration) return;

        publicKey = this._urlB64ToUint8Array(publicKey);

        return registration.pushManager.subscribe({
            userVisibleOnly: true,
            applicationServerKey: publicKey,
        });
    }

    /**
     * @public
     */
    static unsubscribe() {
        const registration = ServiceWorker.getRegistration();

        if (!registration) return

        return registration.pushManager.getSubscription().then(subscription => {
            if (subscription) subscription.unsubscribe();
        });
    }

    /**
     * @public
     */
    static isEnabled() {
        return Notification.permission === this._PERMISSION.GRANTED;
    }

    /**
     * @public
     * @param {String} message
     */
    static notify(message) {
        if (!('Notification' in window)) {
            console.warn('Browser does not support desktop notifications.');

            return;
        }

        if (Notification.permission !== PushNotification._PERMISSION.GRANTED) {
            this.requestPermission().then(permission => {
                this._trySendNotification(message);
            });
        } else {
            this._trySendNotification(message);
        }
    }
}

PushNotification._PERMISSION = {
    GRANTED: 'granted',
};